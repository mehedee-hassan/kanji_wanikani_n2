<a target='_blank'>https://www.wanikani.com/kanji/禁</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/煙</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/静</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/危</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/険</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/関</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/係</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/員</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/転</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/落</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/第</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/石</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/飛</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/駐</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/捨</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/遊</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/泳</a><br/>
<br/>WEEK 1.2<br/>
<a target='_blank'>https://www.wanikani.com/kanji/喫</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/非</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/御</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/常</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/受</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/付</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/案</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/内</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/議</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/化</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/階</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/段</a><br/>
<br/>WEEK 1.3<br/>
<a target='_blank'>https://www.wanikani.com/kanji/営</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/放</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/押</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/準</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/備</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/定</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/流</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/清</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/掃</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/閉</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/点</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/検</a><br/>
<br/>WEEK 1.4<br/>
<a target='_blank'>https://www.wanikani.com/kanji/鉄</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/窓</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/符</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/精</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/算</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/改</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/札</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/線</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/路</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/刻</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/番</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/号</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/快</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/速</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/路</a><br/>
<br/>WEEK 1.5<br/>
<a target='_blank'>https://www.wanikani.com/kanji/港</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/由</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/深</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/降</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/両</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/替</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/賃</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/割</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/増</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/優</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/席</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/側</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/座</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/寄</a><br/>
<br/>WEEK 1.6<br/>
<a target='_blank'>https://www.wanikani.com/kanji/郵</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/局</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/貯</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/包</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/達</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/際</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/初</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/再</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/療</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/科</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/婦</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/婦</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/皮</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/膚</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/救</a><br/>
<br/>WEEK　1.7<br/>

<a target='_blank'>https://www.wanikani.com/kanji/看</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/板</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/羽</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/成</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/第</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/総</a><br/>

<br/>2WEEK<br/>

<br/>WEEK2.1<br/>
<a target='_blank'>https://www.wanikani.com/kanji/普</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/券</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/数</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/機</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/復</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/片</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/枚</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/期</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/販</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/指</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/調</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/整</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/表</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/示</a><br/>
<br/>WEEK2.2<br/>
<a target='_blank'>https://www.wanikani.com/kanji/現</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/支</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/払</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/支</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/預</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/戻</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/残</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/照</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/照</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/硬</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/貨</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/確</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/認</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/違</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/取</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/消</a><br/>
<br/>WEEK2.3<br/>
<a target='_blank'>https://www.wanikani.com/kanji/温</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/冷</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/緑</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/紅</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/玉</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/返</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/団</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/般</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/児</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/幼</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/児</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/歳</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/未</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/満</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/老</a><br/>
<br/>WEEK2.4<br/>
<a target='_blank'>https://www.wanikani.com/kanji/設</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/換</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/向</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/停</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/暖</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/除</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/湿</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/標</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/倍</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/巻</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/録</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/量</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/予</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/約</a><br/>
<br/>WEEK2.5<br/>
<a target='_blank'>https://www.wanikani.com/kanji/帯</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/保</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/留</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/守</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/伝</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/済</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/件</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/信</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/歴</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/箱</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/規</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/変</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/選</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/決</a><br/>
<br/>WEEK2.6<br/>
<a target='_blank'>https://www.wanikani.com/kanji/登</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/編</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/能</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/修</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/完</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/了</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/像</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/類</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/式</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/央</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/存</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/印</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/刷</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/拡</a><br/>

<br/>WEEK2.7<br/>
<a target='_blank'>https://www.wanikani.com/kanji/縮</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/拾</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/鋭</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/鈍</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/肯</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/否</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/浮</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/沈</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/若</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/反</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/賛</a><br/>
３WEEK<br/>
<a target='_blank'>https://www.wanikani.com/kanji/様</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/要</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/利</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/利</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/細</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/在</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/客</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/額</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/込</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/領</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/収</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/械</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/曲</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/汚</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/殿</a><br/>

<br/>WEEK3.2<br/>
<a target='_blank'>https://www.wanikani.com/kanji/連</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/絡</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/荷</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/届</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/参</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/衣</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/他</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/臓</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/凍</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/等</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/配</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/担</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/弁</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/当</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/頂</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/商</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/個</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/相</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/交</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/効</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/限</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/全</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/共</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/忘</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/経</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/過</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/責</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/任</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/負</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/必</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/袋</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/積</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/燃</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/枝</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/葉</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/埋</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/製</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/容</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/器</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/装</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/雑</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/誌</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/資</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/難</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/訓</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/練</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/震</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/加</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/延</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/断</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/管</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/記</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/迷</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/協</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/願</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/平</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/結</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/果</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/封</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/法</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/各</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/位</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/異</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/移</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/務</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/更</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/越</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/郊</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/周</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/畑</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/昨</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/翌</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/軒</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/毒</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/涙</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/笑</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/署</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/依</a><br/>

４WEEK<br/>

<a target='_blank'>https://www.wanikani.com/kanji/頼</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/府</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/到</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/希</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/望</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/申</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/姓</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/宅</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/勤</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/部</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/4.2</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/婚</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/招</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/状</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/喜</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/治</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/委</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/祝</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/舞</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/礼</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/忙</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/妻</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/張</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/奥</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/4.3:</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/浅</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/君</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/久</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/互</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/追</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/伸</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/皆</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/暮</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/慣</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/活</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/恋</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/健</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/康</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/祈</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/4.4:</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/福</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/拝</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/打</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/幸</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/失</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/突</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/然</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/諸</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/情</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/退</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/職</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/紹</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/介</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/4.5:</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/次</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/章</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/対</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/最</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/適</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/誤</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/直</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/例</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/詞</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/形</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/助</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/副</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/囲</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/4.6:</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/夢</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/専</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/史</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/祭</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/査</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/戦</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/将</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/橋</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/憎</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/殺</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/悲</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/恥</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/感</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/球</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/愛</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/仲</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/良</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/4.7:</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/腹</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/背</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/息</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/血</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/圧</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/臓</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/液</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/鼻</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/吸</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/眠</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/欲</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/疲</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/胃</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/胸</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/傾</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/横</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/湯</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/恐</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/原</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/因</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/置</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/寝</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/熱</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/焼</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/灯</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/油</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/余</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/毛</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/糸</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/肌</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/柔</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/香</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/軟</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/溶</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/選</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/接</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/塗</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/緒</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/泥</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/黃</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/途</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/具</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/床</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/壁</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/乾</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/柱</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/虫</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/歯</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/防</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/磨</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/抜</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/悩</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/髪</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/刺</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/肩</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/腰</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/節</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/神</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/痛</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/浴</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/的</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/汗</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/師</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/談</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/燥</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/折</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/賞</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/庫</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/造</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/費</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/可</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/秒</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/身</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/召</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/杯</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/沸</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/粉</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/末</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/栄</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/鳴</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/訪</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/呼</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/警</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/報</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/裏</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/差</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/続</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/辞</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/面</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/操</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/実</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/列</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/棒</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/枯</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/舟</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/貧</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/昔</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/岩</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/泣</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/咲</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/司</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/念</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/涼</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/散</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/吹</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/得</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/告</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/税</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/価</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/格</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/超</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/均</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/靴</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/供</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/象</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/組</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/値</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/募</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/無</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/詰</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/麦</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/純</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/草</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/塩</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/固</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/演</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/菓</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/贈</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/省</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/承</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/破</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/理</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/泉</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/宿</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/季</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/豊</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/富</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/迎</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/泉</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/宿</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/季</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/豊</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/富</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/迎</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/泊</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/居</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/築</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/角</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/徒</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/畳</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/米</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/解</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/辺</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/察</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/役</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/美</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/術</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/坂</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/寺</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/湾</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/島</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/岸</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/園</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/湖</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/城</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/谷</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/財</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/観</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/宝</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/仏</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/王</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/銅</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/塔</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/絵</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/略</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/順</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/版</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/芸</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/複</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/刊</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/層</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/束</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/甘</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/辛</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/皿</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/綿</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/旧</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/厚</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/薄</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/粒</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/極</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/革</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/苦</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/困</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/氷</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/永</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/夫</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/識</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/挟</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/狭</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/群</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/祖</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/偶</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/隅</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/求</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/簡</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/単</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/許</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/給</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/与</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/応</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/課</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/程</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/制</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/講</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/級</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/基</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/導</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/庭</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/育</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/猫</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/探</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/灰</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/輪</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/晩</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/劇</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/公</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/踊</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/種</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/匹</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/渡</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/馬</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/貝</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/酒</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/蒸</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/干</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/竹</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/卵</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/根</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/材</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/植</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/砂</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/乳</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/含</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/炭</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/脂</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/筒</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/卒</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/績</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/論</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/志</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/述</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/構</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/遅</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/仮</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/机</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/冊</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/採</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/濃</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/筆</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/航</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/陸</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/損</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/候</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/船</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/丸</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/混</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/想</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/故</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/乱</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/河</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/輸</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/率</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/宇</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/戸</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/晴</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/曇</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/雪</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/恵</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/陽</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/雲</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/漁</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/底</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/鉱</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/彼</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/滴</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/偉</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/則</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/測</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/定</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/授</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/零</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/州</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/波</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/盗</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/逃</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/疑</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/捕</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/絶</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/党</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/補</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/童</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/爆</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/暴</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/亡</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/罪</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/型</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/欧</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/労</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/兆</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/貿</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/易</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/農</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/命</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/令</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/権</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/億</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/星</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/武</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/巨</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/競</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/敗</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/逆</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/勝</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/投</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/手</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/軍</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/兵</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/倒</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/骨</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/針</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/叫</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/脳</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/抱</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/双</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/尊</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/雇</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/条</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/善</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/律</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/勢</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/怖</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/荒</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/耕</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/景</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/掘</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/批</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/判</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/臣</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/賢</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/勇</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/敬</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/評</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/似</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/犯</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/孫</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/娘</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/覚</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/帽</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/環</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/境</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/減</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/努</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/庁</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/官</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/宇</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/独</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/技</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/政</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/況</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/腕</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/昇</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/幅</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/著</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/占</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/比</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/較</a><br/>
<a target='_blank'>https://www.wanikani.com/kanji/並</a><br/>






署
依
頼
頼
府
到
希
望
申
姓
宅
勤
部
4.2

婚
招
状
喜
治
委
祝
舞
礼
忙
妻
張
奥
4.3:

浅
君
久
互
追
伸
皆
暮
慣
活
恋
健
康
祈

4.4:<br/>
福
拝
打
幸
失
突
然
諸
情
退
職
紹
介

4.5:<br/>
次
章
対
最
適
誤
直
例
詞
形
助
副
囲

4.6:

夢
専
史
祭
査
戦
将
橋
憎
殺
悲
恥
感
球
愛
仲
良

4.7:<br/>

腹
背
息
血
圧
臓
液
鼻
吸
眠
欲
疲
胃
胸


5.1:<br/>

傾
横
湯
恐
原
因
置
寝
熱
焼
灯
油
余

5.2:<br/>

毛
糸
肌
柔
香
軟
溶
選
接
塗
緒
泥
黃

5.3

途
具
床
壁
乾
柱
虫
歯
防
磨
抜
悩
髪


5.4:
刺
肩
腰
節
神
痛
浴
的
汗
師
談
燥
折
5.5

賞
庫
造
費
可
秒
身
召
杯
沸
粉
末
栄

5.6

鳴
訪
呼
警
報
裏
差
続
辞
面
操
実
列

5.7
棒
枯
舟
貧
昔
岩
泣
咲
司
念
涼
散
吹

6.1
得
告
税
価
格
超
均
靴
供
象
組
値
募
無


6.2

詰
麦
純
草
塩
固
演
菓
贈
省
承
破
理



6.3
泉
宿
季
豊
富
迎

6.4
6.5
6.6
6.7
7.1
7.2
7.3
7.4
7.5
7.6
7.7
7.8
8.1
8.2
8.3
8.4
8.5
8.6
8.7





6.3

泉
宿
季
豊
富
迎
泊
居
築
角
徒
畳
米
解

6.4

辺
察
役
美
術
坂
寺
湾
島
岸
園
湖
城
谷

6.5

財
観
宝
仏
王
銅
塔
絵
略
順
版
芸
複
刊


6.6

層
束
甘
辛
皿
綿
旧
厚
薄
粒
極
革
苦

6.7

困
氷
永
夫
識
挟
狭
群
祖
偶
隅


7.1

求
簡
単
許
給
与
応
課
程
制
講
級
基
導

7.2

庭
育
猫
探
灰
輪
晩
劇
公
踊
種
匹
渡
馬

7.3

貝
酒
蒸
干
竹
卵
根
材
植
砂
乳
含
炭
脂

7.4

筒
卒
績
論
志
述
構
遅
仮
机
冊
採
濃
筆

7.5

航
陸
損
候
船
丸
混
想
故
乱
河
輸

7.6

率
宇
戸
晴
曇
雪
恵
陽
雲

7.7


漁
底
鉱
彼
滴
偉
則
測
定
授
零

8.1

州
波
盗
逃
疑
捕
絶
党
補
童
爆
暴
亡
罪

8.2

型
欧
労
兆
貿
易
農
命
令
権
億
星

8.3

武
巨
競
敗
逆
勝
投
手
軍
兵
倒
骨
針
叫

8.4

脳
抱
双
尊
雇
条
善
律
勢
怖
荒
耕
景
掘

8.5


批
判
臣
賢
勇
敬
評
似
犯
孫
娘
覚
帽

8.6

環
境
減
努
庁
官
宇
独
技
政
況
腕

8.7

昇
幅
著
占
比
較
並
